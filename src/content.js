;(function() {
    "use strict";

    var colors = ['#ff5c5c', '#c36161', '#eedddd', '#88aacc', '#5ac25a', '#3cff7d', '#67e1f1'];
    var getAllowOtherLK = false;
    var getAllowShowStatistic = false;
    var paths = [
        '/forum/profile.php',
        '/forum/tracker.php',
        '/forum/viewforum.php',
        '/forum/search.php',
        '/forum/up_torrent.php'
    ];
    var findElements = [
        '.forumline tr td.row1:not([title]) + td.row1 .seedmed b',
        'td.seedmed b',
        'td span.seedmed b',
        '.forumline .row1 .seed',
        '.genmed span.seedmed'
    ];
    var markedElements = [
        'td span.gen',
        'td[nowrap] a.genmed',
        'td span.tDL',
        'td span.gen',
        '.genmed span.seedmed'
    ];
    var markedElementsResult = null;

    function getId(str) {

        return str.replace(/.*u=(\d+).*/, '$1');
    }

    function allowExec() {

        var my_id = getId(document.querySelector('.menutable td a.mainmenu[href^=profile]').getAttribute('href'));

        var allow = false;

        var truePath = location.pathname.replace(/(\/.+\.php).*/, "$1");

        paths.forEach(function(e, i) {

            if(
                (i==0 && (getAllowOtherLK || getId(location.search.toString()) == my_id)) ||
                (i > 0 && e == truePath)
            ) {

                allow = true;
            }
        });

        return allow;
    }

    function findColumns(callback) {

        var elems = [];

        paths.forEach(function(e, i) {

            if(location.pathname.replace(/(\/.+\.php).*/, "$1") == e) {

                elems = document.querySelectorAll(findElements[i]);

                callback(elems);
            }
        });

    }

    function getColumns(callback) {

        if (typeof callback != 'null' && callback != 'null') {

            if(markedElementsResult === null) {

                paths.forEach(function(e, i) {

                    if(location.pathname == e) {

                        markedElementsResult = i;
                    }
                });
            }

            if(markedElementsResult !== null) {

                return callback.closest('tr').querySelector(markedElements[markedElementsResult]).closest('td');
            }
        }
    }

    function getExtData(data, callback) {

        chrome.runtime.sendMessage(data, function(response) {

            callback(response);
        });
    }

    function ready() {

        getExtData({'type': 'getAllData', 'colors': colors}, function(data) {

            if(data && typeof data['allow_other_lk'] != 'undefined' && data['allow_other_lk']) {

                getAllowOtherLK = true;
            }

            if(data && typeof data['allow_statistic'] != 'undefined' && data['allow_statistic']) {

                getAllowShowStatistic = true;
            }

            var i = 0;
            var color;
            while(i < colors.length) {

                color = data['colors'][i];
                if(typeof color != 'undefined' && color != null && color != 'null') {

                    colors[i] = data['colors'][i];
                }
                i++;
            }

            (function() {

                if(allowExec()) {

                    findColumns(function(elems) {

                        //var max = 0;
                        var style = '';
                        var targetTR = false;
                        var statistic = [0,0,0,0,0,0,0];
                        var colspan = 7;

                        if(getAllowShowStatistic && elems && typeof elems[0] != "undefined") {

                            colspan = 10;
                            targetTR =
                                location.pathname == paths[0] ?
                                elems[0].closest('tr') :
                                elems[0].closest('tr').previousSibling.previousSibling;
                        }

                        elems.forEach(function(e) {

                            var p = getColumns(e);
                            var color = '';

                            style = '';

                            //var pears = [16, 12, 6, 4, 2, 1, 0];
                            var seeds = e.innerText;

                            switch(true) {
                                case seeds >= 16:
                                    statistic[0]++;
                                    color = colors[0];
                                    break;
                                case seeds >= 12:
                                    statistic[1]++;
                                    color = colors[1];
                                    break;
                                case seeds >= 6:
                                    statistic[2]++;
                                    color = colors[2];
                                    break;
                                case seeds >= 4:
                                    statistic[3]++;
                                    color = colors[3];
                                    break;
                                case seeds >= 2:
                                    statistic[4]++;
                                    color = colors[4];
                                    break;
                                case seeds == 1:
                                    statistic[5]++;
                                    color = colors[5];
                                    break;
                                default:
                                    statistic[6]++;
                                    color = colors[6];
                            }

                            if(p.getAttribute('style') != null) {

                                style = p.getAttribute('style');
                            }

                            p.setAttribute('style', 'background: ' + color + ';' + style);
                        });

                        // выводим статистику
                        if(targetTR) {

                            var element = document.createElement('tr');

                            element.innerHTML = '<td colspan="' + colspan + '"><table width="100%">'
                                + '<tr>'
                                    + '<td align="center" class="row1" colspan="' + colspan + '" style="font-size: 15px; padding: 2px;">Краткая статистика по раздачам (верхний слотбец - количество раздающих, нижний количество раздач)</td>'
                                + '</tr>'
                                + '<tr>'
                                    + getRow('&gt;=16')
                                    + getRow('12-15')
                                    + getRow('6-11')
                                    + getRow('4-5')
                                    + getRow('2-3')
                                    + getRow('1')
                                    + getRow('0')
                                + '</tr>'
                                + '<tr>'
                                    + getRow(statistic[0])
                                    + getRow(statistic[1])
                                    + getRow(statistic[2])
                                    + getRow(statistic[3])
                                    + getRow(statistic[4])
                                    + getRow(statistic[5])
                                    + getRow(statistic[6])
                                + '</tr>'
                                + '</table>';

                            targetTR.parentNode.insertBefore(element, targetTR);
                        }

                    });
                }
            })();
        });
    }

    function getRow(text) {

        return '<td align="center" class="nnm-helper-mark-bnt row1">' + text + '</td>';
    }

    if(/nnm.?club\.??/.test(location.hostname)) {

        ready();
    }
})();