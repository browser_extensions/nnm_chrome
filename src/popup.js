;(function() {
    "use strict";

    document.addEventListener('DOMContentLoaded', function() {

        document.getElementById('allow-other-users').addEventListener('change', function() {

            localStorage['sttv-allow-other-users'] = (this.checked ? '1' : '');
        });

        document.getElementById('allow-show-statistic').addEventListener('change', function() {

            localStorage['sttv-allow-show-statistic'] = (this.checked ? '1' : '');
        });

        function getColor(i) {

            var selector = 'sttv-color-' + i;
            return localStorage[selector];
        }

        function addColorListener(selector, i) {

            document.querySelector(selector).addEventListener('input', function() {

                selector = 'sttv-' + selector;
                localStorage['sttv-color-' + i] = this.value;

                return this.value;
            });
        }

        function setColor(selector, i) {

            var color = getColor(i);

            //console.log(document.querySelector(selector).value);

            if(color != '' && typeof color != 'undefined') {

                document.querySelector(selector).value = color;
            }
        }

        document.getElementById('allow-other-users').checked = localStorage['sttv-allow-other-users'];
        document.getElementById('allow-show-statistic').checked = localStorage['sttv-allow-show-statistic'];

        var i = 0;
        while (i < 7) {

            var selector = '#color-' + i + ' input';
            setColor(selector, i);
            addColorListener(selector, i);
            i++;
        }
    });
})();