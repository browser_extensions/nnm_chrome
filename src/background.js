;(function() {
    "use strict";

    chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {

        if(typeof request.type !== 'undefined') {

            switch (request.type) {
                case 'getColor':
                    sendResponse({'color': localStorage['sttv-color-' + request.color_id], 'id': request.color_id});
                    break;
                case 'getAllowOtherLK':
                    sendResponse(localStorage['sttv-allow-other-users']);
                    break;
                case 'getAllowShowStatistic':
                    sendResponse(localStorage['sttv-allow-show-statistic']);
                    break;
                case 'getAllData':
                    var data = {};

                    data['allow_other_lk'] = localStorage['sttv-allow-other-users'];
                    data['allow_statistic'] = localStorage['sttv-allow-show-statistic'];
                    data['colors'] = [];

                    if (typeof request.colors !== 'undefined') {
                        request.colors.forEach(function (e, i) {

                            data['colors'].push(localStorage['sttv-color-' + i]);
                        });
                    }

                    sendResponse(data);
                break;
            }
        }
    });
})();